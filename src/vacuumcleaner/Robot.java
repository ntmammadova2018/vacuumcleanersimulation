/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacuumcleaner;

import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import static vacuumcleaner.RobotUIController.robot;

/**
 *
 * @author User
 */
public class Robot {

    //rectangle and its dimensions
    private Rectangle rectangle;
    public static final int rectHeight = 80;
    public static final int rectWidth = 80;

    //steplength of robot
    private int stepLength = 80;

    //next positions of robot
    private int nextX;
    private int nextY;

    //direction labels
    static final int left = 1;
    static final int right = 2;
    static final int up = 3;
    static final int down = 4;


    public Robot(Pane pane) {
        //create rectangle set dimensions
        rectangle = new Rectangle();
        rectangle.setHeight(rectHeight);
        rectangle.setWidth(rectWidth);

        //set image for robot using rectangle
        Image img = new Image("vacuumcleaner/robot.png");
        rectangle.setFill(new ImagePattern(img));

        //initial positions of rectangle
        rectangle.setY(0);
        rectangle.setX(0);

        //add rectangle to pane
        pane.getChildren().add(rectangle);
    }

     /**    Move according to the given direction
     * @param direction - direction to where the robot should move
     */   
    
    public void generalMove(int direction) {
        if (!getCurrentState().isIsFinalState()) {
            switch (direction) {
                case left:
                    this.moveLeft();
                    break;
                case right:
                    this.moveRight();
                    break;
                case up:
                    this.moveUp();
                    break;
                case down:
                    this.moveDown();
                    break;
                default:
                    break;
            }                        
        }
    }
     /**    Check if exits block
     * @param x - coordinate of the state on X axis
     * @param y - coordinate of the state on Y axis
     */
    public boolean existsBlock(int x, int y) {
        State state = ReInforcementLearning.stateMatrix[x][y];
        if (state != null && state.isIsBlock()) {
            return true;
        }
        return false;
    }
    
    //      Get  and Set X and Y positions of robot
    public int getPosX() {
        return (int) this.rectangle.getX();
    }

    public void setPosX(int posX) {
        this.rectangle.setX(posX);
    }

    public int getPosY() {
        return (int) this.rectangle.getY();
    }

    public void setPosY(int posY) {
        this.rectangle.setY(posY);;
    }

    //      Move robot if does not exist any block
    public void moveRight() {

        this.nextX = this.getPosX() + stepLength;
        if (nextX <= Main.windowWidth - rectWidth) {
            if (!existsBlock(this.nextX, this.getPosY())) {

                this.setPosX(nextX);

            }
        }
    }

    public void moveLeft() {
        this.nextX = this.getPosX() - stepLength;
        if (nextX >= 0) {
            if (!existsBlock(this.nextX, this.getPosY())) {

                this.setPosX(nextX);

            }
        }
    }

    public void moveUp() {
        this.nextY = this.getPosY() - stepLength;
        if (nextY >= 0) {
            if (!existsBlock(this.getPosX(), this.nextY)) {

                this.setPosY(nextY);

            }
        }
    }

    public void moveDown() {
        this.nextY = this.getPosY() + stepLength;
        if (nextY <= Main.windowHeight - rectHeight) {
            if (!existsBlock(this.getPosX(), this.nextY)) {

                this.setPosY(nextY);

            }
        }
    }
    //          Get the state where the robot currently is
    public State getCurrentState() {
        State state = ReInforcementLearning.stateMatrix[this.getPosX()][this.getPosY()];
        return state;
    }

}
