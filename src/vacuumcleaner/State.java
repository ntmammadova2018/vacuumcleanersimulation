/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacuumcleaner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author User
 */
public class State {

    //Q values of a state
    HashMap<Integer, Double> actionsQValue;

    //Actions which were explored during learning
    public List<Integer> exploredActions;

    //direction labels
    public static final int left = 1;
    public static final int right = 2;
    public static final int up = 3;
    public static final int down = 4;

    //parameters for checking state types
    private boolean isFinalState;
    private boolean isPitfall;
    private boolean isBlock;

    //parameters of a state
    public int x;
    public int y;
    public int id;

    public State(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
        actionsQValue = new HashMap<>();
        exploredActions = new ArrayList();
        actionsQValue.put(left, 0.0);
        actionsQValue.put(right, 0.0);
        actionsQValue.put(up, 0.0);
        actionsQValue.put(down, 0.0);

    }

    public State(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Give reward based on state
     *
     * @param previous - the state for which Q-value will be updated
     */
    public int getReward(State previous) {
        int reward = 0;


        if (this.isIsFinalState()) {            
            // Reward for reaching final state
            reward = 90;

//

        } else if (this.isIsPitfall()) {
            // Reward for falling into pitfall
            reward = -90;

        }else {
      // Reward for each step
            reward = -1;
        }        
        return reward;
    }

    //      Check whether the current state is pitfall
    public boolean isIsPitfall() {
        return isPitfall;
    }

    //      Assign the current state to pitfall
    public void setIsPitfall(boolean isPitfall) {
        this.isPitfall = isPitfall;
    }

    //      Check whether the current state is final
    public boolean isIsFinalState() {
        return isFinalState;
    }

    //      Assign the current state to final
    public void setIsFinalState(boolean isFinalState) {
        this.isFinalState = isFinalState;
    }    

    //      Check whether the current state has block
    public boolean isIsBlock() {
        return isBlock;
    }

    //      Put a block to the current state
    public void setIsBlock(boolean isBlock) {
        this.isBlock = isBlock;
    }

    /**
     * Get a state by its ID
     *
     * @param id - the ID of a state which is searched
     */
    public static State getStateById(int id) {
        for (int v = 0; v < Main.windowWidth; v = v + Robot.rectWidth) {
            for (int j = 0; j < Main.windowHeight; j = j + Robot.rectHeight) {
                State state = ReInforcementLearning.stateMatrix[v][j];
                if (state != null && state.id == id) {
                    return state;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "State{" + "x = " + x + ", y = " + y + ", id = " + id + '}';
    }
}
