/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacuumcleaner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author saria
 */
/*
Main class where the application will start
 */
public class Main extends Application {

    public static final int windowWidth = 800;
    public static final int windowHeight = 480;
    public static Stage window;
    public static Scene mainScene;
    public static Parent root;

    @Override       //Setting window for UI
    public void start(Stage primaryStage) throws Exception {
        root = FXMLLoader.load(getClass().getResource("RobotUI.fxml"));
        Scene scene = new Scene(root, windowWidth, windowHeight+100);
        
        mainScene = scene;
        window = primaryStage;
        primaryStage.setTitle("Robot Simulation");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args); //setting program to JavaFX
    }

}
