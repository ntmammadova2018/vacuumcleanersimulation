/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vacuumcleaner;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
//import static vacuumcleaner.ReInforcementLearning.exploredStates;
import static vacuumcleaner.Robot.rectHeight;
import static vacuumcleaner.Robot.rectWidth;

/**
 * FXML Controller class
 *
 * @author saria
 */
public class RobotUIController implements Initializable {

    public Pane pane;
    Random r = new Random();

    //store blocks in arraylist
    public static List<State> Blocks = new ArrayList();

    //pitfall
    State pitfallState;
    State pitfallState1;
    //final state
    State finalState;

    //Robot object
    public static Robot robot;
    ReInforcementLearning rf;

    private static final int BlockCount = 10;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pane.setFocusTraversable(true);

        //create grid
        setGrid();
        setControls();

        //intialize robot
        robot = new Robot(pane);
        rf = new ReInforcementLearning();
        //learn
        learnEnvironment();
        //install event handler
        installEventHandler(pane);

    }

    //keyboard interactions
    private void installEventHandler(final Node keyNode) {
        final EventHandler<KeyEvent> keyEventEventHandler = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (null != event.getCode()) {
                    switch (event.getCode()) {
                        case DOWN:
                            robot.moveDown();
                            break;
                        case UP:
                            robot.moveUp();
                            break;
                        case LEFT:
                            robot.moveLeft();
                            break;
                        case RIGHT:
                            robot.moveRight();
                            break;
                        case R: //for reinforecement learning
                            robot.generalMove(rf.getPolicy(robot.getCurrentState(), rf.getMaxQ(robot.getCurrentState())));
                            break;
                        case N: //for a new stage
                        {
                            try {
                                refresh(Main.mainScene, pane);
                            } catch (IOException ex) {
                                Logger.getLogger(RobotUIController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        break;
                        default:
                            break;

                    }
                }

            }

        };
        keyNode.setOnKeyPressed(keyEventEventHandler);
    }

    //learn environment in 50000 iterations
    private void learnEnvironment() {

        for (int i = 0; i < 5000; i++) {
            for (int z = 0; z < Main.windowWidth; z = z + Robot.rectWidth) {
                for (int j = 0; j < Main.windowHeight; j = j + Robot.rectHeight) {
                    State st = ReInforcementLearning.stateMatrix[z][j];
                    if (st != null) {
                        st.exploredActions.clear();
                    }
                }
            }
            robot.setPosX(0);
            robot.setPosY(0);

            while (!robot.getCurrentState().isIsFinalState()) {
                int action = rf.getAction(robot.getCurrentState());
                State previous = robot.getCurrentState();
                robot.generalMove(action);
                // setGirlState(robot.existsRobot(robot.getPosX(), robot.getPosY()));
                rf.updateQ(previous, action, robot.getCurrentState());
            }
        }
        robot.setPosX(0);
        robot.setPosY(0);
    }

    //refresh scene
    public void refresh(Scene scene, Pane pane) throws IOException {
        Pane rpane = FXMLLoader.load(getClass().getResource("RobotUI.fxml"));
        pane.getChildren().setAll(rpane);

    }

    //get random locations 
    public static int getRandomDivisible(int lowerBound, int upperBound) {
        if (lowerBound > 0) {
            lowerBound += (rectHeight - 1);
        }
        if (upperBound < 0) {
            upperBound -= (rectHeight - 1);
        }

        lowerBound /= rectHeight;
        upperBound /= rectHeight;

        int n = upperBound - lowerBound + 1;

        if (n < 1) {
            throw new IllegalArgumentException("Range too small");
        }

        return rectHeight * (lowerBound + new Random().nextInt(n));
    }

    //set pitfall in a random state
    public void setPitfall() {
        Random rand = new Random();
        int rndX = rand.nextBoolean() ? 0 : Main.windowWidth - Robot.rectWidth;
        int rndY = getRandomDivisible(Robot.rectWidth, Main.windowHeight - 2 * Robot.rectHeight);
        pitfallState = new State(rndX, rndY);
        rndX = rand.nextBoolean() ? 0 : Main.windowWidth - Robot.rectWidth;
        rndY = getRandomDivisible(Robot.rectWidth, Main.windowHeight - 2 * Robot.rectHeight);
        pitfallState1 = new State(rndX, rndY);

    }

    //set random blocks in random states
    public void setBlocks() {
        Blocks.clear();
        for (int i = 0; i < BlockCount; i++) {

            int rndX = getRandomDivisible(Robot.rectWidth, Main.windowWidth - Robot.rectWidth);
            int rndY = getRandomDivisible(Robot.rectHeight, Main.windowHeight - Robot.rectHeight);
            //State state=

            State state = new State(rndX, rndY);
            Blocks.add(state);
        }

        // System.out.println(Blocks);
    }

    //set final state (partially random)
    public void setFinalState() {
        int rndX = getRandomDivisible(0, Main.windowWidth - Robot.rectWidth);
        int rndY = Main.windowHeight - Robot.rectHeight;
        finalState = new State(rndX, rndY);
    }

    //create grid, set block, set final state
    private void setGrid() {
        /*Setting states on the grid*/
        setFinalState();
        setPitfall();
        setBlocks();
        /*Assigning coordinates to the states*/
        int n = 0;

        int m = Blocks.size();

        for (int i = 0; i < Main.windowWidth; i = i + Robot.rectWidth) {
            for (int j = 0; j < Main.windowHeight; j = j + Robot.rectHeight) {

                Rectangle rect = new Rectangle();
                rect.setX(i);
                rect.setY(j);
                rect.setWidth(Robot.rectWidth);
                rect.setHeight(Robot.rectHeight);

                n++;
                State state = new State(n, i, j);
                ReInforcementLearning.stateMatrix[i][j] = state;
                rect.setFill(Color.BURLYWOOD);
                rect.setStroke(Color.GRAY);
                if (j == finalState.y && i == finalState.x) {
                    rect.setFill(Color.GREEN);
                    state.setIsFinalState(true);

                } else if ((i == pitfallState.x && j == pitfallState.y)
                        || (i == pitfallState1.x && j == pitfallState1.y)) {

                    Image img = new Image("vacuumcleaner/flame.jpg");
                    rect.setFill(new ImagePattern(img));
                    state.setIsPitfall(true);
                } else if (m > 0) {
                    for (int h = 0; h < Blocks.size(); h++) {
                        if (i == Blocks.get(h).x && j == Blocks.get(h).y) {
                            rect.setFill(Color.BLACK);
                            state.setIsBlock(true);
                             m--;
                        }
                    }
                }

                pane.getChildren().add(rect);
            }

        }

    }

    //set labels for UI
    private void setControls() {
        Text label = new Text("To see extracted policy press 'R'");
        Text label1 = new Text("To generate a new game press 'N'");
        label.setTranslateX(0);
        label.setTranslateY(Main.windowHeight + 30);
        label.setStyle("-fx-font: 24 arial;");
        label.setFill(Color.BROWN);
        label1.setTranslateX(0);
        label1.setTranslateY(Main.windowHeight + 60);
        label1.setStyle("-fx-font: 24 arial;");
        label1.setFill(Color.BROWN);
        pane.getChildren().addAll(label, label1);

    }
}
